(function () {
    var usuariosFactory =   function ($http)  {
        var factoria = {};

        factoria.postUsuario = function (usuario) {
            return $http.post('/api/usuarios/', usuario);
        }
        return factoria;
    };

    angular.module("appTuCuenta")
        .factory('usuariosFactory', usuariosFactory);
}());