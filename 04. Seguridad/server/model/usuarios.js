var util = require("../util");
module.exports = function Proyectos() {
    var coleccion = "usuarios";
    var sesiones = [];

    return {
        postUsuario: function (doc, cb) {
            util.conectarMongo(coleccion, function (err, collection) {
                doc.idSesion = Math.random() * (88888) + 11111;
                doc.timeStamp = new Date();
                sesiones.push(doc.idSesion);
                collection.insert(doc, cb);
            })

        },
        postSesion: function (doc, cb) {
            util.conectarMongo(coleccion, function (err, collection) {
                // Encontrar usuario
                collection.findOne({
                        email: doc.email,
                        password: doc.password
                    },
                    function (err, docDB) {
                        if (err) cb(err, null);
                        // Crear sesion 
                        docDB.idSesion = Math.random() * (88888) + 11111;
                        docDB.timeStamp = new Date();
                        sesiones.push(docDB.idSesion);
                        // Actualizar base de datos
                        collection.save({
                            _id: docDB.id
                        }, cb);
                    })
            })
        },

        getSesion: function (idSesion) {
            var sesionEncontrada = sesiones.filter(function (sesion) {
                return sesion == idSesion;
            })[0];

        }
    };
};