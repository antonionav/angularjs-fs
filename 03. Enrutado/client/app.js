angular.module("appTuCuenta", ['ngResource','ngRoute']);

angular.module('appTuCuenta').config(function ($routeProvider) {
    $routeProvider
        .when('/', {
           controller: 'InicioCtrl',
            controllerAs: 'vm',
            templateUrl: 'inicio/inicio.html'
        })
        .when('/proyectos/nuevo', {
            controller: 'ProyectosCtrl',
            controllerAs: 'vm',
            templateUrl: 'proyectos/nuevoProyecto.html'
        })
        .when('/proyectos/lista', {
            controller: 'ProyectosCtrl',
            controllerAs: 'vm',
            templateUrl: 'proyectos/listaProyectos.html'
        })
        .otherwise({
            redirectTo: '/'
        });
});