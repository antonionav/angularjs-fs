var express = require('express');
var router = express.Router();
var Proyectos = require("./model/proyectos");
var util = require("./util");

var mongoDB = "mongodb://localhost:27017/AppTuCuenta";
var proyectos = new Proyectos(mongoDB);

router.route('/api/proyectos')
    .get(function (req, res, next) {
        proyectos.getProyectos(function (err, docs) {
            if (err) util.tratarError(err, res);
            res.json(docs);
        });
    })
    .post(function (req, res, next) {
        var doc = req.body;
        proyectos.postProyecto(doc, function (err, docs) {
            if (err) util.tratarError(err, res);
            res.status(200).json(doc);
        });
    });

module.exports.router = router;