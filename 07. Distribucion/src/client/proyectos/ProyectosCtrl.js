(function () {
    var ProyectosCtrl = function (proyectosFactory, validacionesFactory, $rootScope) {
        var vm = this;
        vm.titulo = "Proyectos";
        vm.proyectos = proyectosFactory.query();
        vm.guardarProyecto = guardarProyecto;

        function guardarProyecto() {
            if (validacionesFactory.esRangoValido(
                vm.nuevoProyecto.fechaInicio, vm.nuevoProyecto.fechaFin)) {
                vm.nuevoProyecto.manager = $rootScope.nombre;
                vm.nuevoProyecto.manager = idUsuario;
                var nuevoProyecto = new proyectosFactory(vm.nuevoProyecto);
                nuevoProyecto.$save();
            } else {
                vm.rangoFechasInvalido = true;
            }
        }
    }
    angular
        .module("appTuCuenta")
        .controller("ProyectosCtrl", ProyectosCtrl);
}());