var express = require('express');
var router = express.Router();
var Proyectos = require("../model/proyectos");
var Usuarios = require("../model/usuarios");
var util = require("./util");
var ObjectId = require('mongodb').ObjectID;

var usuarios = new Usuarios(util);
var proyectos = new Proyectos(util,Usuarios,ObjectId);




router.route('/api/usuarios')
    .post(function (req, res, next) {
        var usuario = req.body;
        usuarios.postUsuario(usuario, function (err, docs) {
            if (err) {
                util.tratarError(err, res);
            } else {
                res.json(docs[0]);
            }
        });
    });
router.route('/api/sesiones')
    .post(function (req, res, next) {
        var doc = req.body;
        usuarios.postSesion(doc, function (err, doc) {
            if (err) {
                util.tratarError(err, res);
            } else {
                util.test(JSON.stringify(doc));
                res.json(doc);
            }
        });
    });

router.route('/api/priv/menu')
    .get(function (req, res, next) {
        var rol = res.usuario.rol;
        switch (rol) {
        case 'supervisor':
            res.json([{
                texto: 'Inicio',
                ruta: '#/',
                loc: '/'
            }]);
            break;
        case 'manager':
            res.json([{
                    texto: 'Inicio',
                    ruta: '#/',
                    loc: '/'
            },
                {
                    texto: 'Nuevo Proyecto',
                    ruta: '/#/proyectos/nuevo',
                    loc: '/proyectos/nuevo'
            },
                {
                    texto: 'Lista de proyectos',
                    ruta: '/#/proyectos/lista',
                    loc: '/proyectos/lista'
            }]);
            break;
        case 'cliente':
            res.json([{
                    texto: 'Inicio',
                    ruta: '#/',
                    loc: '/'
            },
                {
                    texto: 'Lista de proyectos',
                    ruta: '/#/proyectos/lista',
                    loc: '/proyectos/lista'
            }]);
            break;
        }
    });





router.route('/api/priv/proyectos')
    .get(function (req, res, next) {
        proyectos.getProyectos(res.usuario, function (err, docs) {
            if (err) {
                util.tratarError(err, res);
            } else {
                util.test("Buscando proyectos para : " + JSON.stringify(res.usuario.email) + " encontrados: " + docs.length);
                res.json(docs);
            }
        });
    })
    .post(function (req, res, next) {
        var doc = req.body;
        proyectos.postProyecto(res.usuario, doc, function (err, doc) {
            if (err) {
                util.tratarError(err, res);
            } else {
                util.test("post:/api/priv/proyectos  " + JSON.stringify(doc));
                res.json(doc);
            }
        });
    }).put(function (req, res, next) {
        var doc = req.body;
        proyectos.putProyecto(res.usuario, doc, function (err, doc) {
            if (err) {
                util.tratarError(err, res);
            } else {
                util.test("put:/api/priv/proyectos  " + JSON.stringify(doc));
                res.json(doc);
            }
        });
    });

router.route('/api/priv/proyectos/:_id')
    .get(function (req, res, next) {
        var _id = req.params._id
        proyectos.getProyecto(_id, res.usuario, function (err, doc) {
            if (err) {
                util.tratarError(err, res);
            } else {
                util.test("Buscando proyecto"+ _id + "para : " + JSON.stringify(res.usuario.email) + " encontrado: " + JSON.stringify(doc));
                res.json(doc);
            }
        });
    })
    .put(function (req, res, next) {
        var _id = req.params._id
        var doc = req.body;
        console.log("Cambiando el proyecto" + _id + " con datos " + JSON.stringify(doc));
        res.send("ok");
    });

// Registro


module.exports.router = router;