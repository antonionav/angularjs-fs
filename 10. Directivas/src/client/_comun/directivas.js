(function () {
    angular.module('comun-Directivas', [])
        .directive('cabecera', function () {
            return {
                restrict: 'AE',
                replace: 'true',
                template: '<span><h2>AppTuCuenta</h2><h3>Gestiona tus proyectos con nosotros.</h3></span>'
            };
        }).directive('pie', function () {
            return {
                restrict: 'AEM',
                replace: 'false',
                templateUrl: '_comun/pie.html'
            };
        }).directive('mensaje', function () {
            return {
                restrict: 'E',
                templateUrl: '_comun/mensaje.html',
                replace: true,
                transclude: true
            };
        }).directive('button', function () {
            return {
                restrict: 'E',
                compile: function (element, attributes) {
                    element.addClass('btn');
                    if (attributes.type === 'submit') {
                        element.addClass('btn-primary');
                    }
                    if (attributes.size) {
                        element.addClass('btn-' + attributes.size);
                    }
                }
            };
        }).directive('resaltado', function () {
            return {
                link: function ($scope, element, attrs) {
                    element.bind('mouseenter', function () {
                        element.css('background-color', 'yellow');
                    });
                    element.bind('mouseleave', function () {
                        element.css('background-color', 'white');
                    });
                }
            };
        }).directive('filaProyecto', function () {
            return {
                restrict: 'AE',
                replace: 'true',
                templateUrl: 'proyectos/filaProyecto.html',
                scope: {
                    proyecto: "=proj"
                }
            };
        });
}());