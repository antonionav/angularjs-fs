(function () {
    var MenuCtrl = function ($location,$rootScope, menuFactory) {
        var vm = this;
        vm.isActive = function (ruta) {
            return ruta === $location.path();
        }

        $rootScope.opcionesMenu = menuFactory.query();
    }
    angular
        .module('appTuCuenta')
        .controller('MenuCtrl', MenuCtrl);
}());



