(function () {
    var ProyectoCtrl = function ($routeParams, $cookieStore, proyectosFactory) {
        var vm = this;
        var idProyecto = $routeParams.idProyecto;

        proyectosFactory.get({
            _id: idProyecto
        }, function (res) {
            vm.proyecto = res;
            console.log($cookieStore.get("usuario"));
            console.log(vm.proyecto);
            console.log(vm.proyecto.estado);
            var rol = $cookieStore.get("usuario").rol;
            vm.activarContratar = rol === "manager" && !vm.proyecto.estado;
            vm.activarValidar = rol === "cliente" && vm.proyecto.estado === "Contratado";
            vm.activarPagar = rol === "manager" && vm.proyecto.estado === "Validado";
        });


        vm.contratarProyecto = function () {
            vm.proyecto.estado = "Contratado";
            console.log("Contratando: " + JSON.stringify(vm.proyecto))
            proyectosFactory.update(vm.proyecto);
        }
        vm.validarProyecto = function () {
            vm.proyecto.estado = "Validado";
            console.log("Validando: " + JSON.stringify(vm.proyecto))
            proyectosFactory.update(vm.proyecto);
        }
        vm.pagarProyecto = function () {
            vm.proyecto.estado = "Pagado";
            console.log("Pagando: " + JSON.stringify(vm.proyecto))
            proyectosFactory.update(vm.proyecto);
        }
    }
    angular
        .module("appTuCuenta")
        .controller("ProyectoCtrl", ProyectoCtrl);
}());