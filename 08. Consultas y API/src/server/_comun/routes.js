var express = require('express');
var router = express.Router();
var Proyectos = require("../model/proyectos");
var Usuarios = require("../model/usuarios");
var util = require("./util");


var proyectos = new Proyectos();
var usuarios = new Usuarios();



router.route('/api/priv/proyectos')
    .get(function (req, res, next) {
        proyectos.getProyectos(res.usuario, function (err, docs) {
            if (err) {
                util.tratarError(err, res);
            } else {
                res.json(docs);
            }
        });
    })
    .post(function (req, res, next) {
        var doc = req.body;
        proyectos.postProyecto(res.usuario, doc, function (err, doc) {
            if (err) {
                util.tratarError(err, res);
            } else {
                util.test("post:/api/priv/proyectos  " + JSON.stringify(doc));
                res.json(doc);
            }
        });
    });

router.route('/api/priv/proyectos/:_id')
    .get(function (req, res, next) {
        var _id = req.params._id     
        proyectos.getProyecto(_id, res.usuario, function (err, doc) {
            if (err) {
                util.tratarError(err, res);
            } else {
                res.json(doc);
            }
        });
    })
    .put(function (req, res, next) {
        var _id = req.params._id 
        var doc = req.body;
        console.log("Cambiando el proyecto" + _id + " con datos " + JSON.stringify(doc));
        res.send("ok");
    });

// Registro
router.route('/api/usuarios')
    .post(function (req, res, next) {
        var usuario = req.body;
        usuarios.postUsuario(usuario, function (err, docs) {
            if (err) {
                util.tratarError(err, res);
            } else {
                res.json(docs[0].idSesion);
            }
        });
    });
// Login
router.route('/api/sesiones')
    .post(function (req, res, next) {
        var doc = req.body;
        usuarios.postSesion(doc, function (err, doc) {
            if (err) {
                util.tratarError(err, res);
            } else {
                util.test(JSON.stringify(doc));
                res.json(doc.idSesion);
            }
        });
    });

module.exports.router = router;