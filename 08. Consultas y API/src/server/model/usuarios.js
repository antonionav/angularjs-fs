var util = require("../_comun/util");
module.exports = function Usuarios() {
    var coleccion = "usuarios";

    function postUsuario(usuario, cb) {
        getUsuarioByEmail(usuario.email, function (err, usuarioAnterior) {
            if (err) cb(err, null);
            if (usuarioAnterior) {
                cb(new Error("El Usuario ya existe"), null);
            } else {
                util.conectarMongo(coleccion, function (err, usuarios) {
                    if (err) cb(err, null);
                    usuario.idSesion = Math.floor(Math.random() * (88888) + 11111);
                    usuario.timeStamp = new Date();
                    if(usuario.rol === undefined)
                    {
                        if(usuario.email==="manager@escuela.it") usuario.rol="manager";
                        if(usuario.email==="super@escuela.it") usuario.rol="supervisor";
                    }
                    usuarios.insert(usuario, cb);
                })
            }
        })
    };

    function postSesion(doc, cb) {
        util.conectarMongo(coleccion, function (err, collection) {
            // Encontrar usuario
            collection.findOne({
                    email: doc.email,
                    password: doc.password
                },
                function (err, docDB) {
                    if (err) {
                        cb(err, null);
                    } else {
                        if (docDB) {
                            docDB.idSesion = Math.floor(Math.random() * (88888) + 11111);
                            docDB.timeStamp = new Date();
                            // Actualizar base de datos
                            collection.save(docDB, function(err, doc){
                                console.log("guardado y devolviendo: " + JSON.stringify(docDB));
                                cb(null,docDB);
                            });
                        } else {
                            cb(new Error("Credencial incorrecta"), null);
                        }
                    }
                })
        })
    };

    function getUsuarioByEmail(email, cb) {
        util.conectarMongo(coleccion, function (err, collection) {
            collection.findOne({
                email: email
            }, cb);
        })
    };

    function getSesion(idSesionBuscada, res, next, cb) {
        util.conectarMongo(coleccion, function (err, collection) {
            collection.findOne({
                idSesion: parseInt(idSesionBuscada)
            }, function (err, usuario) {
                if (err) cb(err, res, next, null);
                //To Do: Si se encontró usuario, ampliar timeout en 20 minutos
                cb(null, res, next, usuario);
            });

        })
    };

    return {
        postUsuario: postUsuario,
        postSesion: postSesion,
        getSesion: getSesion
    };

};