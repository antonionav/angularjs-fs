var util = require("../_comun/util");
var Usuarios = require("./usuarios");
var ObjectId = require('mongodb').ObjectID;

module.exports = function Proyectos() {
    var coleccion = "proyectos";


    // TODO: buscar tambien si soy cliente
    function getProyectos(usuario, cb) {
        util.test("Buscando proyectos para :" + JSON.stringify(usuario));
        util.conectarMongo(coleccion, function (err, collection) {
            collection.find({
                manager: usuario.email
            }).toArray(cb);
        })
    }

    function getProyecto(_id, usuario, cb) {
        var q= {_id: new ObjectId(_id), manager: usuario.email };
        util.test("Buscando el proyecto " + _id + " para : " + JSON.stringify(usuario.email) + " q: " + JSON.stringify(q));
        util.conectarMongo(coleccion, function (err, collection) {
            collection.findOne(q, cb);
        })
    }

    function postProyecto(usuario, doc, cb) {
        if (!doc.manager) doc.manager = usuario.email;
        if (doc.manager === usuario.email) {
            util.test("Guardando el proyecto para " + JSON.stringify(doc));
            util.conectarMongo(coleccion, function (err, collection) {
                collection.findOne({
                    manager: usuario.email,
                    nombre: doc.nombre
                }, function (err, docDB) {
                    if (err) {
                        cb(err, null);
                    } else {
                        if (docDB) {
                            util.test("Ya existe otro proyecto con ese nombre:  " + JSON.stringify(doc));
                            cb(new Error("Ya existe otro proyecto con ese nombre"), null);
                        } else {
                            collection.insert(doc, function (err, docsDB) {
                                if (err) {
                                    cb(err, null);
                                } else {
                                    var docDB = docsDB[0];
                                    util.test("Insertado proyecto:  " + JSON.stringify(docDB));
                                    var cliente = {
                                        nombre: doc.cliente,
                                        email: doc.cliente,
                                        password: doc.cliente,
                                        rol: "cliente"
                                    };
                                    var usuarios = new Usuarios();
                                    usuarios.postUsuario(cliente, function (err, usuarioDB) {
                                        if (err) {
                                            if (err.toString().indexOf("El Usuario ya existe") >= 0) {
                                                util.test("El cliente ya existe el proyecto va bien:  " + JSON.stringify(usuarioDB));
                                                cb(null, docDB);
                                            } else {
                                                util.test("Eliminando proyecto por no poder guardar:  " + JSON.stringify(cliente));
                                                collection.remove({
                                                    id: docDB._id
                                                }, function (err, result) {
                                                    if (err) {
                                                        cb(err, null);
                                                    } else {
                                                        util.test("No se ha guardado el proyecto pues no se guardó el cliente:  " + JSON.stringify(cliente));
                                                        cb(new Error("No se ha guardado el proyecto pues no se guardó el cliente"), null);
                                                    }
                                                })
                                                cb(err, null);
                                            }
                                        } else {
                                            util.test("Insertado cliente:  " + JSON.stringify(usuarioDB));
                                            cb(null, docDB);
                                        }
                                    })
                                }
                            });
                        }
                    }
                })
            })
        } else {
            util.test("Sin manager o incorrecto:  " + JSON.stringify(doc));
            cb(new Error("Sin manager o incorrecto"), null);
        }
    }


    return {
        getProyectos: getProyectos,
        getProyecto: getProyecto,
        postProyecto: postProyecto
    };
};