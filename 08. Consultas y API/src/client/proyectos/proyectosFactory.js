(function () {
    var proyectosFactory =   function ($resource)  {
        return $resource("/api/priv/proyectos/:_id", {});
    };

    angular.module("appTuCuenta").factory('proyectosFactory',proyectosFactory);
}());