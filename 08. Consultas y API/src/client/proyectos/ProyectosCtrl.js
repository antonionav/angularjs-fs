(function () {
    var ProyectosCtrl = function (proyectosFactory, validacionesFactory, $rootScope) {
        var vm = this;
        vm.titulo = "Proyectos";
        vm.nuevoProyecto = {
            fechaInicio: new Date(),
            fechaFin: new Date()
        };
        vm.proyectos = proyectosFactory.query();
        vm.guardarProyecto = guardarProyecto;

        function guardarProyecto() {
            vm.proyectoGuardado = false;
            if (validacionesFactory.esRangoValido(
                vm.nuevoProyecto.fechaInicio, vm.nuevoProyecto.fechaFin)) {
                vm.nuevoProyecto.manager = $rootScope.nombre;
                var nuevoProyecto = new proyectosFactory(vm.nuevoProyecto);
                nuevoProyecto.$save(function (res) {
                    vm.nuevoProyecto._id = res._id;
                    vm.proyectoGuardado = true;
                });

            } else {
                vm.rangoFechasInvalido = true;
            }
        }
    }
    angular
        .module("appTuCuenta")
        .controller("ProyectosCtrl", ProyectosCtrl);
}());