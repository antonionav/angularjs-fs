(function () {
    var sesionesFactory =   function ($http)  {
        var factoria = {};

        factoria.postUsuario = function (usuario) {
            return $http.post('/api/sesiones/', usuario);
        }
        return factoria;
    };

 angular
 .module("appTuCuenta")
 .factory('sesionesFactory',sesionesFactory);
}());