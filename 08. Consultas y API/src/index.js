var config = require("./server/_comun/config");
var util = require("./server/_comun/util");
var server = require('./server/_comun/routes');
var express = require('express');
var bodyParser = require('body-parser');
util.test('ready');
var configuracion = config.configuracion();
util.test(JSON.stringify(configuracion));

    var Usuarios = require("./server/model/usuarios");
    var usuarios = new Usuarios();

    var app = express(); app.use(bodyParser.json()); app.use(express.static(__dirname + '/client'));

    // Middleware de validación de sesiones
    app.use('/api/priv/', function (req, res, next) {
        var idSesion = req.get('idSesion');
        usuarios.getSesion(idSesion, res, next, util.controlDeSesion);
    });

    app.use('/', server.router); util.test('steady');

    app.listen(3000); util.test('go');