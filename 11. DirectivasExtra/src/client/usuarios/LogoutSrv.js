(function () {
    var LogoutSrv = function ($q, $cookieStore, $location, $http, $window) {
        
        $cookieStore.remove('idSesion');
        $http.get('/#/')
            .success(function(){
                $location.path('/#/');
                $window.location.href='/#/';
                $window.location.reload();
            });
        return $q.promise;
    }

    angular
        .module("appTuCuenta")
        .service('LogoutSrv', LogoutSrv);
}());