(function () {
    var ProyectosCtrl = function (proyectosFactory, validacionesFactory, $rootScope, $http) {
        var vm = this;

        vm.nuevoProyecto = {
            fechaInicio: new Date(),
            fechaFin: new Date()
        };

        vm.numTotalProyectos = 0;
        vm.proyectosPorPagina = 3;
        
        vm.pagination = {current: 1};

        vm.cambiarPagina = function(pagina){
            getProyectosPagina(pagina);
        }
        function getProyectosPagina(pagina) {
            $http.get('/api/priv/proyectos/paginas/' + pagina)
                .then(function (result) {
                    vm.proyectos = result.data;
                    console.log(vm.proyectos);
                });
        }

        function getnumTotalProyectos() {
            $http.get('/api/priv/proyectos/total/num')
                .then(function (result) {
                    vm.numTotalProyectos = result.data;
                    console.log(vm.numTotalProyectos);
                });
        }
        
        vm.guardarProyecto = guardarProyecto;

        vm.claseEstado = function (proyecto) {
            if (!proyecto || !proyecto.estado) {
                return "text-danger"
            }
            if (proyecto.estado === "Contratado") {
                return "text-warning"
            }
            if (proyecto.estado === "Validado") {
                return "text-info"
            }
            if (proyecto.estado === "Pagado") {
                return "text-success"
            }

        }

        function guardarProyecto() {
            vm.proyectoGuardado = false;
            if (validacionesFactory.esRangoValido(
                vm.nuevoProyecto.fechaInicio, vm.nuevoProyecto.fechaFin)) {
                vm.nuevoProyecto.manager = $rootScope.nombre;
                var nuevoProyecto = new proyectosFactory(vm.nuevoProyecto);
                nuevoProyecto.$save(function (res) {
                    vm.nuevoProyecto._id = res._id;
                    vm.proyectoGuardado = true;
                });

            } else {
                vm.rangoFechasInvalido = true;
            }
        }
        
        getnumTotalProyectos();
        getProyectosPagina(1);
    }
    angular
        .module("appTuCuenta")
        .controller("ProyectosCtrl", ProyectosCtrl);
}());