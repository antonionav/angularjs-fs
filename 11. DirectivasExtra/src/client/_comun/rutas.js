(function () {
    var rutas = function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'InicioCtrl',
                controllerAs: 'vm',
                templateUrl: 'inicio/inicio.html'
            })
            .when('/usuarios/registro', {
                controller: 'UsuariosCtrl',
                controllerAs: 'vm',
                templateUrl: 'usuarios/registro.html'
            })
            .when('/usuarios/login', {
                controller: 'UsuariosCtrl',
                controllerAs: 'vm',
                templateUrl: 'usuarios/login.html'
            })
            .when('/usuarios/logout.html', {
                resolve: {
                    logout: 'LogoutSrv'
                }
            })
            .when('/proyectos/nuevo', {
                controller: 'ProyectosCtrl',
                controllerAs: 'vm',
                templateUrl: 'proyectos/nuevoProyecto.html'
            })
            .when('/proyectos/lista', {
                controller: 'ProyectosCtrl',
                controllerAs: 'vm',
                templateUrl: 'proyectos/listaProyectos.html'
            })
            .when('/proyectos/:idProyecto', {
                controller: 'ProyectoCtrl',
                controllerAs: 'vm',
                templateUrl: 'proyectos/proyecto.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    };

    angular.module('appTuCuenta').config(rutas);
}());