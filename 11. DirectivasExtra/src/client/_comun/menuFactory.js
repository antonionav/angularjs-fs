(function () {
    var menuFactory =   function ($resource)  {
        return $resource("/api/priv/menu/", {});
    };

    angular.module("appTuCuenta").factory('menuFactory',menuFactory);
}());