(function () {
    var MenuCtrl = function ($location,$rootScope, $cookieStore, menuFactory) {
        var vm = this;
        var logged = ($cookieStore.get('idSesion')) ? true : false;
                
        vm.isActive = function (ruta) {
            return ruta === $location.path();
        }

        if (logged) {
            $rootScope.opcionesMenu = menuFactory.query();
        } else {
            $rootScope.opcionesMenu = [{
                    texto: 'Inicio',
                    ruta: '#/',
                    loc: '/'
            },{
                    texto: 'Login',
                    ruta: '/#/usuarios/login',
                    loc: '/usuarios/login'
            }];
        }
    }
    angular
        .module('appTuCuenta')
        .controller('MenuCtrl', MenuCtrl);
}());



