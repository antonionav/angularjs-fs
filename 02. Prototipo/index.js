var util = require("./util/util.js");
var routes = require('./routes/routes');
var express = require('express');
var bodyParser = require('body-parser');
util.test('ready');

var app = express();
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));
app.use('/', routes.router);
app.get('/test', function (req, res, next) {
    res.send('<h1>AppTuCuenta</h1><p>NodeJS y Express funcionan</p>');
});

util.test('steady');
app.listen(3000);
util.test('go');