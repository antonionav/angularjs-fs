module.exports = function Proyectos(mongoDB) {
    var MongoClient = require('mongodb').MongoClient;
    var colName = "proyectos";

    function connect(cb) {
        MongoClient.connect(mongoDB, function (err, db) {
            if (err) cb(err, null);
            console.log("Conectado a: " + mongoDB);
            var collection = db.collection(colName);
            cb(null, collection);
        })
    };

    return {
        getProyectos: function (cb) {
            connect(function (err,collection) {
                collection.find().toArray(cb);
            })

        },
        postProyecto: function (doc, cb) {
            connect(function (err, collection) {
                collection.insert(doc, cb);
            })
        }
    };
};