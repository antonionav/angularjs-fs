var config = require("./server/_comun/config");
var util = require("./server/_comun/util");
var routes = require('./server/_comun/routes');
var express = require('express');
var bodyParser = require('body-parser');
util.test('ready');
var configuracion = config.configuracion();

util.test(JSON.stringify(configuracion));

var Usuarios = require("./server/model/usuarios");
var usuarios = new Usuarios(util);

var app = express();
var server = require("http").Server(app);
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));

// Middleware de validación de sesiones
app.use('/api/priv/', function (req, res, next) {
    var idSesion = req.get('idSesion');
    usuarios.getSesion(idSesion, res, next, util.controlDeSesion);
});

app.use('/', routes.router);
util.test('steady');


// PULL -- recoger ir a buscar

// PUSH -- empujar




// *********
// Socket.io
// *********
var Proyectos = require("./server/model/proyectos");
var ObjectId = require('mongodb').ObjectID;
var proyectos = new Proyectos(util, Usuarios, ObjectId);

var io = require("socket.io")(server);

var handleClient = function (socket) {
    console.log("conectados!!" );

    socket.on("newProj", function (data) {
        console.log("newProj: " + data);
        proyectos.getStatusProyectos(function (err, statusProyectos) {
            console.log("emitiendo: " + JSON.stringify(statusProyectos));
            socket.emit("StatusProyectos", {
                statusProyectos: statusProyectos
            });
            socket.broadcast.emit("StatusProyectos", {
                statusProyectos: statusProyectos
            });
        });
    });

};
io.on("connect", handleClient);
// *********

server.listen(3000);
util.test('go');