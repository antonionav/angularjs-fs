(function () {
    var CuadromandoCtrl = function ($scope, socketFactory) {
        var vm = this;

        socketFactory.emit("newProj", 0);
        socketFactory.on('StatusProyectos', function (data) {
            vm.listo = false;
            $scope.$apply();
            vm.numProyectos = data.statusProyectos.length;
            vm.statusProyectos = data.statusProyectos;

            vm.rowsIngresos = [];
            vm.rowsGastos = [];

            vm.statusProyectos.forEach(function (object) {
                vm.rowsIngresos.push({
                    c: [{
                            v: object.nombre
                        }, {
                            v: object.presupuesto
                        },
                    ]
                });
                vm.rowsGastos.push({
                    c: [{
                            v: object.nombre
                        }, {
                            v: object.presupuesto
                        },
                        {
                            v: object.coste
                        },
                    ]
                });
            });

            vm.chartIngresos = {};
            vm.colsIngresos = [{
                    id: "t",
                    label: "Proyecto",
                    type: "string"
                    }, {
                    id: "s",
                    label: "Ingresos",
                    type: "number"
                    }
                ];
            vm.chartIngresos.data = {
                "cols": vm.colsIngresos,
                "rows": vm.rowsIngresos
            };
            vm.chartIngresos.type = "BarChart";
            vm.chartIngresos.options = {
                'title': 'Ingresos por proyecto',
                colors: ['#0000FF']
            }


            vm.chartGastos = {};
            vm.colsGastos = [{
                    id: "t",
                    label: "Proyecto",
                    type: "string"
                    }, {
                    id: "s",
                    label: "Ingresos",
                    type: "number"
                    },
                {
                    id: "s",
                    label: "Gastos",
                    type: "number"
                    }
                ];
            vm.chartGastos.data = {
                "cols": vm.colsGastos,
                "rows": vm.rowsGastos
            };
            vm.chartGastos.type = "ColumnChart";
            vm.chartGastos.options = {
                'title': 'Gastos por proyecto',
                colors: ['#00FF00','#FF0000']
            }
            vm.listo = true;
        });


    }
    angular
        .module('appTuCuenta')
        .controller('CuadromandoCtrl', CuadromandoCtrl);
}());