module.exports.test = function (value) {
    console.log("Desde nodemon: " + value);
}

module.exports.tratarError = function (err, res) {
    if(err) console.log(err);
    if(res )res.status(500).send(err);
}

var MongoClient = require('mongodb').MongoClient;
var mongoDB = "mongodb://localhost:27017/AppTuCuenta";
module.exports.conectarMongo = 
    function connect(coleccion, callBackFunction ) {
        MongoClient.connect(mongoDB, function (err, db) {
            if (err) {
                cb(err, null);
            }
            var collection = db.collection(coleccion);
            callBackFunction(null, collection);
        })
    };


module.exports.controlDeSesion = function (err, res, next, usuario) {
    if (err) res.status(500).send(err);
    if (usuario) {
        console.log('usuario encontrado ;-)');
        if ((new Date() - usuario.timeStamp) > 20 * 60 * 1000) {
            console.log(
                'Sesión caducada:' + JSON.stringify(usuario));
            res.status(419).send('Sesión caducada');
        } else {
            usuario.timeStamp = new Date();
            // To Do: actualizar la base de datos
            console.log('sesion activa y continuamos');
            res.usuario = usuario;
            next();
        }
    } else {
        console.log('usuario no encontrado :-(');
        res.status(401).send('Credencial inválida');
    }
}