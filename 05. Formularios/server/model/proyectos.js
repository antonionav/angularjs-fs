var util = require("../util");
module.exports = function Proyectos() {
    var coleccion = "proyectos";
    
    return {
        getProyectos: function (cb) {
            util.conectarMongo(coleccion,function (err,collection) {
                collection.find().toArray(cb);
            })
        },
        postProyecto: function (doc, cb) {
            util.conectarMongo(coleccion, function (err, collection) {
                collection.insert(doc, cb);
            })
        }
    };
};