(function () {
    var UsuariosCtrl = function ($rootScope, $location, $cookieStore, usuariosFactory, sesionesFactory) {
        var vm = this;
        vm.usuario = {};
        vm.sesion = {};

        vm.registro = function () {
            if (vm.usuario.email.indexOf("gmail")>0) {
                vm.emailInvalido = true;
            } else {
                
                vm.emailInvalido = false;
                usuariosFactory.postUsuario(vm.usuario)
                    .success(function (res) {
                        var idSesion = res;
                        console.log("idSesion recibida: " + idSesion);
                        $rootScope.nombre = vm.usuario.email;
                        $rootScope.mensaje = 'recién creado';
                        $cookieStore.put("idSesion", idSesion);
                        $location.path("/");
                    }).error(function (err) {
                        console.log("error al recibir idSesion: " + err);
                    });
            }
        };

        vm.login = function () {
            sesionesFactory.postUsuario(vm.sesion)
                .success(function (res) {
                    var nuevoUsuario = res[0];
                    console.log(res);
                    $rootScope.nombre = vm.sesion.email;
                    $rootScope.mensaje = 'recién creado';
                    $cookieStore.put("idSesion", nuevoUsuario.idSesion);
                    $location.path("/");
                });
        }
    }

    angular
        .module("appTuCuenta")
        .controller('UsuariosCtrl', UsuariosCtrl);
}());