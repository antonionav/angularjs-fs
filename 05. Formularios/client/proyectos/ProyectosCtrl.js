(function () {
    var ProyectosCtrl = function (proyectosFactory) {
        var vm = this;
        vm.titulo = "Proyectos";
        vm.proyectos = proyectosFactory.query();
        vm.guardarProyecto = guardarProyecto;

        function guardarProyecto() {
            var nuevoProyecto = new proyectosFactory(vm.nuevoProyecto);
            nuevoProyecto.$save();
        }

    }
    angular
    .module("appTuCuenta")
    .controller("ProyectosCtrl", 
                ['proyectosFactory', ProyectosCtrl]);
}());

