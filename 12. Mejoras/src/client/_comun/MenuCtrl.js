(function () {
    var MenuCtrl = function ($location, $rootScope, $cookieStore, menuFactory) {
        var vm = this;
        vm.isActive = function (ruta) {
            return ruta === $location.path();
        }

        vm.logOut = function () {
            console.log("saliendo");
            $cookieStore.remove("idSesion");
            $cookieStore.remove("usuario");
            $location.path('/usuarios/login');
        }

        $rootScope.opcionesMenu = menuFactory.query();
    }
    angular
        .module('appTuCuenta')
        .controller('MenuCtrl', MenuCtrl);
}());