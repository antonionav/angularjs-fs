(function () {
    var ProyectoCtrl = function ($routeParams, $cookieStore, proyectosFactory) {
        var vm = this;
        var idProyecto = $routeParams.idProyecto;

        vm.noSoyCliente = ($cookieStore.get("usuario").rol != "cliente");
        vm.noSoyManager = ($cookieStore.get("usuario").rol != "manager");
        
        proyectosFactory.get({
            _id: idProyecto
        }, function (res) {
            vm.proyecto = res;
            console.log($cookieStore.get("usuario"));
            console.log(vm.proyecto);
            console.log(vm.proyecto.estado);
            var rol = $cookieStore.get("usuario").rol;
            vm.activarContratar = rol === "manager" && !vm.proyecto.estado;
            vm.activarValidar = rol === "cliente" && vm.proyecto.estado === "Contratado";
            vm.activarValorar = rol === "cliente" && vm.proyecto.estado === "Validado";
            vm.activarPagar = rol === "manager" && vm.proyecto.estado === "Validado";
        });


        vm.contratarProyecto = function () {
            vm.proyecto.estado = "Contratado";
            console.log("Contratando: " + JSON.stringify(vm.proyecto))
            proyectosFactory.update(vm.proyecto);
        }
        vm.validarProyecto = function () {
            vm.proyecto.estado = "Validado";
            console.log("Validando: " + JSON.stringify(vm.proyecto))
            proyectosFactory.update(vm.proyecto);
        }
         vm.valorarProyecto = function () {
            console.log("Valorando: " + JSON.stringify(vm.proyecto))
            proyectosFactory.update(vm.proyecto);
        }
        vm.pagarProyecto = function () {
            vm.proyecto.estado = "Pagado";
            console.log("Pagando: " + JSON.stringify(vm.proyecto))
            proyectosFactory.update(vm.proyecto);
        }
    }
    angular
        .module("appTuCuenta")
        .controller("ProyectoCtrl", ProyectoCtrl);
}());